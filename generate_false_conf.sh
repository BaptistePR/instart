#!/bin/bash

# to prevent misrunning this command
if [[ "$#" == "3" ]] && [[ "$1" == "generate" ]] && [[ "$2" == "false" ]] && [[ "$3" == "conf" ]]; then
    echo 'export const MONGO = "mongodb+srv://admin:admin@test-uf5dz.mongodb.net/test?retryWrites=true"' > ./conf/env.js
    echo 'export const JWT_SECRET = "nooope"' >> ./conf/env.js
    echo 'export const PORT = 5001' >> ./conf/env.js
fi
