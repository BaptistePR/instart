# INSTART-BACK

[![pipeline status](https://gitlab.com/BaptistePR/instart/badges/master/pipeline.svg)](https://gitlab.com/BaptistePR/instart/commits/master) [![coverage report](https://gitlab.com/BaptistePR/instart/badges/master/coverage.svg)](https://gitlab.com/BaptistePR/instart/commits/master)

  
Back of instaZZ

## URLs

All urls start with `.../instazz/1.0.0/`

### Login

POST :
* URL : `login`
* BODY : The model of a [user](markdown-header-users)

### Users

User model
```js
var UserSchema = new Schema({
    username: String,
    password: String,
})
```

GET :
* URL : `users?id=...`, `users?username=...` or `users?start=...&length=...`
* BODY : not needed

POST :
* URL : `users`
* BODY : user to be added (username needs to be uniq)

DELETE : 
* URL : `users`
* BODY : id of the user to be deleted

### Posts

Post model
```js
var PostSchema = new Schema({
    username: String,
    title: String,
    description: String,
})
```

GET :
* URL : `posts?id=...`, `posts?username=...`, `posts?start=...&length=...` or `posts?start=...&length=...&username=...`
* BODY : not needed

POST a new post without image :
* URL : `posts`
* BODY : post to be added (username needs to exist)

POST the image of an existin post :
* URL : `posts/upload`
* BODY (multiform, data named `file`) : the image to upload

DELETE : 
* URL : `posts`
* BODY : id of the post to be deleted

### Coms

Com model
```js
var ComsSchema = new Schema({
    postID: String,
    username: String,
    description: String,
    date: Date,
})
```

GET :
* URL : `coms?id=...`, `coms?username=...`, `coms?start=...&length=...`, `coms?start=...&length=...&username=...` or `coms?postID=...`
* BODY : not needed

POST :
* URL : `coms`
* BODY : post to be added (username needs to exist and so does postID)

DELETE : 
* URL : `coms`
* BODY : id of the post to be deleted

### Likes

Like model
```js
var LikesSchema = new Schema({
    ownerID: String,
    isOnPost: Boolean,
    username: String,
})
```

GET :
* URL : `likes?ownerID=...` or `likes?ownerID=...&username=...`
* BODY : not needed

POST :
* URL : `likes`
* BODY : post to be added (username needs to exist and so does ownerID)

DELETE : 
* URL : `likes`
* BODY : id of the post to be deleted