#!/bin/bash

if [ "$#" == "0" ]; then
    ./node_modules/.bin/eslint $(find ./components -name '*.js') \
			       $(find ./middleWare -name '*.js') \
			       'app.js'
else
    ./node_modules/.bin/eslint $*
fi
