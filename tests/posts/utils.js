function postsAreEquals(post1, post2){
    return (!post1 && !post2)
        || (post1 && post2
            && post1.username == post2.username
            && post1.title == post2.title
            && post1.description == post2.description)
}

function postsContains(posts, post){
    if(posts){
        for(let i = 0; i < posts.length; ++i){
            if(postsAreEquals(posts[i], post)){
                return true
            }
        }
    }

    return false
}

function postsAreAllDifferents(posts){
    if(posts){
        for(let i = 0; i < posts.length - 1; ++i){
            for(let j = i + 1; j < posts.length; ++j){
                if(postsAreEquals(posts[i], posts[j])){
                    return false
                }
            }
        }
    }
    return true
}

export default {
    postsAreEquals,
    postsContains,
    postsAreAllDifferents,
}
