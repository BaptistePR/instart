import polyfill from "babel-polyfill"
import Posts from "../../components/posts/services"
import Coms from "../../components/coms/services"
import Likes from "../../components/likes/services"
import { expect } from "chai"
import Constants from "../constants.test"
import utils from "./utils"

let res = {
    download: (url) => {
        return url
    }
}

// tests
describe("Posts::services", () => {
    let req = {
        query: {
            id: ""
        }
    }
    
    it("get all posts", async () => {
        let posts
        let post

        posts = await Posts.getAllPosts()
        expect(utils.postsAreAllDifferents(posts)).to.be.true
        expect(posts.length).to.be.equal(Constants.Posts.length)
        for(let i = 0; i < posts.length; ++i){
            expect(utils.postsContains(Constants.Posts, posts[i])).to.be.true
        }

        post = Constants.Posts[0]
        posts = await Posts.getAllPosts(post)
        expect(utils.postsAreAllDifferents(posts)).to.be.true
        expect(posts.length).to.be.equal(1)
        expect(utils.postsAreEquals(post, posts[0])).to.be.true


        post = {username: "bah non", title: "toujours pas", description: "et non"}
        posts = await Posts.getAllPosts(post)
        expect(posts).to.be.deep.equal([])
    })

    it("get one post", async () => {
        let post

        for(let i = 0; i < Constants.Posts.length; ++i){
            post = await Posts.getOnePost(Constants.Posts[i])
            expect(utils.postsAreEquals(post, Constants.Posts[i])).to.be.true
        }

        post = await Posts.getOnePost({username: "bah non"})
        expect(!post).to.be.true
    })

    it("is valide post", async () => {
        let post = {}
        
        expect(await Posts.isValidePost(post)).to.be.false

        post.username = "undefined username"
        expect(await Posts.isValidePost(post)).to.be.false

        post.title = "pololo"
        expect(await Posts.isValidePost(post)).to.be.false

        post.creationDate = new Date()
        expect(await Posts.isValidePost(post)).to.be.false

        post.username = "pololo"
        expect(await Posts.isValidePost(post)).to.be.true
    })

    it("get", async () => {
        let posts = await Posts.getAllPosts()
        
        for(let i = 0; i < posts.length; ++i){
            req.query.id = posts[i]._id
            expect(utils.postsAreEquals(posts[i], await Posts.get(req))).to.be.true
        }

        req.query.id = "nope"
        expect(!(await Posts.get(req))).to.be.true
    })

    it("getAll", async () => {
        let posts = await Posts.getAllPosts()

        // working uniq getAll
        for(let i = 0; i < posts.length; ++i){
            req.query.username = posts[i].username
            let post = await Posts.getAll(req)
            expect(post).not.to.be.equal(undefined)
            expect(post.length).to.be.equal(1)
            expect(utils.postsAreEquals(posts[i], post[0])).to.be.true
        }

        // not working getAll
        req.query.username = "undefined username"
        expect(await Posts.getAll(req)).to.be.equal(undefined)

        // working getAll sorted by date
        req.query = {}
        posts = await Posts.getAll(req)
        expect(posts.length).to.be.equal(Constants.Posts.length)
        for(let i = 0; i < posts.length - 1; ++i){
            expect(posts[i].creationDate >= posts[i + 1].creationDate).to.be.true
        }
    })

    it("add", async () => {
        let dataTest = [
            {
                post: {
                    username: "pololo",
                },
                result: undefined,
            },
            {
                post: Constants.Posts[0],
                result: "same",
            },
        ]

        for(let i = 0; i < dataTest.length; ++i){
            let post = dataTest[i].post
            let result = dataTest[i].result

            if(result === "same"){
                expect(post.username).to.be.equal((await Posts.add(post)).username)
                let posts = await Posts.getAllPosts({username: post.username})
                expect(posts).not.to.be.equal(undefined)
            }else{
                expect(await Posts.add(post)).to.be.equal(result)
            }
        }
    })

    it("remove", async () => {
        let posts = await Posts.getAllPosts()
        
        expect((await Posts.remove({username: "not defined"})).deletedCount).to.be.equal(0)
        expect((await Posts.remove({username: Constants.Posts[0].username})).deletedCount).to.be.equal(1)
        expect(utils.postsContains(await Posts.getAllPosts(), Constants.Posts[0])).to.be.false

        for(let i = 1; i < Constants.Posts.length; ++i){
            expect((await Posts.remove({username: Constants.Posts[1].username})).deletedCount).to.be.equal(1)
        }

        let coms = await Coms.getAllComs()
        let likes = await Likes.getAllLikes()
        for(let i = 0; i < posts.length; ++i){
            for(let j = 0; j < coms.length; ++j){
                expect(posts[i]._id.equals(coms[j].postID)).to.be.false
            }
            
            for(let j = 0; j < likes.length; ++j){
                expect(posts[i]._id.equals(likes[j].ownerID)).to.be.false
            }
        }
    })

    it("load image", async () => {
        for(let i = 0; i < Constants.Posts.length; ++i){
            expect(await Posts.loadImage(res, Constants.Posts[i])).to.be.equal("resources/" + Constants.Posts[i].img)
        }
    })

    it("set property", async () => {
        let posts = await Posts.getAllPosts()
        let id = posts[0]._id
        let coms = posts[0].coms

        expect(await Posts.setProperty(Constants.Users[0]._id, "username", (username) => {return username + "1"})).to.be.equal(3)
        expect(await Posts.setProperty(id, "pololo", (username) => {return username + "1"})).to.be.equal(2)
        expect(await Posts.setProperty(id, "coms", (c) => {return c + 1})).to.be.equal(1)

        let post = await Posts.getOnePost({_id: id})
        expect(post.coms).to.be.equal(coms + 1)
    })

    it("dec coms", async () => {
        let posts = await Posts.getAllPosts()
        let id = posts[0]._id
        let coms = posts[0].coms
        
        expect(await Posts.decComs(Constants.Users[0]._id)).to.be.equal(3)
        expect(await Posts.decComs(id)).to.be.equal(1)

        let post = await Posts.getOnePost({_id: id})
        expect(post.coms).to.be.equal(coms - 1)
    })

    it("dec likes", async () => {
        let posts = await Posts.getAllPosts()
        let id = posts[0]._id
        let likes = posts[0].likes
        
        expect(await Posts.decLikes(Constants.Users[0]._id)).to.be.equal(3)
        expect(await Posts.decLikes(id)).to.be.equal(1)

        let post = await Posts.getOnePost({_id: id})
        expect(post.likes).to.be.equal(likes - 1)
    })

    it("inc likes", async () => {
        let posts = await Posts.getAllPosts()
        let id = posts[0]._id
        let likes = posts[0].likes
        
        expect(await Posts.incLikes(Constants.Users[0]._id)).to.be.equal(3)
        expect(await Posts.incLikes(id)).to.be.equal(1)

        let post = await Posts.getOnePost({_id: id})
        expect(post.likes).to.be.equal(likes + 1)
    })
})
