import polyfill from "babel-polyfill"
import Post from "../../components/posts/controller"
import PostService from "../../components/posts/services"
import UserService from "../../components/users/services"
import PostModel from "../../components/posts/model"
import { expect } from "chai"
import Constants from "../constants.test"
import utils from "./utils"

let res = {
    status: (value) => {
	return {
	    send: (object) => {
		return {
		    status: value,
		    object: object,
		}
	    }
	}
    },
    download: (url) => {
        return url
    }
}

// tests
describe("Posts::controller", () => {    
    it("get", async () => {
        let req = {}
        let result
        let posts = await PostService.getAllPosts()
        posts = posts.sort((p1, p2) => { return p1.creationDate < p2.creationDate })

        // no query
        req.query = {}
        result = await Post.get(req, res)
        expect(result.status).to.be.equal(400)

        // testing id
        req.query = {}
        for(let i = 0; i < posts.length; ++i){
            req.query.id = posts[i]._id
            result = await Post.get(req, res)
            expect(result.status).to.be.equal(200)
            expect(utils.postsAreEquals(posts[i], result.object)).to.be.true
        }
        req.query.id = "nop nop nop"
        result = await Post.get(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object).to.be.deep.equal([])

        // testing username
        req.query = {}
        for(let i = 0; i < posts.length; ++i){
            req.query.username = posts[i].username
            result = await Post.get(req, res)
            expect(result.status).to.be.equal(200)
            expect(utils.postsAreEquals(posts[i], result.object[0])).to.be.true
        }
        req.query.username = "nop nop nop"
        result = await Post.get(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object).to.be.deep.equal([])

        // testing range
        req.query = {}
        req.query.start = 0
        req.query.length = posts.length
        result = await Post.get(req, res)
        expect(result.object).to.be.deep.equal(posts)
        
        req.query.start = 0
        req.query.length = posts.length - 1
        result = await Post.get(req, res)
        expect(result.object).to.be.deep.equal(posts.slice(0, req.query.length))
        
        req.query.start = 1
        req.query.length = posts.length - 1
        result = await Post.get(req, res)
        expect(result.object).to.be.deep.equal(posts.slice(1, 1 + req.query.length))

        // testing range & username
        req.query = {}
        req.query.start = 0
        req.query.length = posts.length
        for(let i = 0; i < posts.length; ++i){
            req.query.username = posts[i].username
            result = await Post.get(req, res)
            expect(result.status).to.be.equal(200)
            expect(utils.postsAreEquals(posts[i], result.object[0])).to.be.true
        }

        // other failing cases
        req.query = {}
        req.query.id = "no"
        req.query.start = "no"
        result = await Post.get(req, res)
        expect(result.status).to.be.equal(400)

        req.query.id = undefined
        result = await Post.get(req, res)
        expect(result.status).to.be.equal(400)
    })
    
    describe("add", () => {        
        it("post", async () => {
            let req = {query:{}}
            let result
            
            // when it fails
            result = await Post.add(req, res)
            expect(result.status).to.be.equal(400)

            // when it succeed
            req.body = {}
            req.body.username = Constants.Users[0].username
            req.body.title = "pololo"
            req.body.creationDate = new Date()
            result = await Post.add(req, res)
            expect(result.status).to.be.equal(200)
            expect(result.object.username).to.be.equal(req.body.username)
        })

        it("post file", async () => {
            let post = await PostService.getOnePost({img: "lion.jpg"})
            let id = post._id
            let users = await UserService.getAllUsers()
            let result
            let req = {
                query: {
                    id: users[0]._id
                },
                file: {
                    path: "./resouces/1234",
                    originalname: "lion.jpg",
                    filename: "1234",
                }
            }

            result = await Post.add(req, res)
            expect(result.status).to.be.equal(200)
            expect(result.object).to.be.deep.equal([])

            req.query.id = id
            await Post.add(req, res)
            post = await PostService.getOnePost({_id: id})
            expect(post.img).to.be.equal("1234.jpg")
        })
    })
    
    it("remove", async () => {
        let req = {body: {username: "not found"}}
        let result

        // when it fails
        result = await Post.remove(req, res)
        expect(result.status).to.be.equal(400)

        // when it succeed
        let posts = await PostService.getAllPosts()
        req.body._id = posts[0]._id
        result = await Post.remove(req, res)
        console.log(result)
        expect(result.status).to.be.equal(200)
        expect(result.object.deletedCount).to.be.equal(1)
    })

    it("get image", async () => {
        let req = {query:{}}
        let postNoImage = (await PostService.getAllPosts({img: undefined}))[0]
        let post = await PostService.getOnePost({img: "lion.jpg"})
        let result

        result = await Post.getImage(req, res)
        expect(result.status).to.be.equal(400)

        req.query.id = postNoImage._id
        result = await Post.getImage(req, res)
        expect(result.status).to.be.equal(400)

        req.query.id = post._id
        result = await Post.getImage(req, res)
        expect(result).to.be.equal("resources/" + post.img)
    })
})
