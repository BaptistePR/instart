import polyfill from "babel-polyfill"
import Query from "../../middleWare/QueryTools"
import { expect } from "chai"

describe("Query", () => {
    it("apply rule", () => {
	expect(Query.applyRule([1, 2, 3], (i) => i > 0)).to.equal(true)
	expect(Query.applyRule([], (i) => i > 0)).to.equal(true)
	expect(Query.applyRule([1, -2, 3], (i) => i > 0)).to.equal(false)
    })
    
    it("are defined", () => {
	expect(Query.areDefined([])).to.equal(true)
	expect(Query.areDefined([1, 2, 3])).to.equal(true)
	expect(Query.areDefined([1, undefined, 3])).to.equal(false)
    })

    it("are undefined", () => {
	expect(Query.areUndefined([])).to.equal(true)
	expect(Query.areUndefined([undefined, undefined, undefined])).to.equal(true)
	expect(Query.areUndefined([undefined, 1, undefined])).to.equal(false)
    })
})
