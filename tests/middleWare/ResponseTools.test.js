import polyfill from "babel-polyfill"
import Answer from "../../middleWare/ResponseTools"
import { expect } from "chai"

let res = {
    status: (value) => {
	return {
	    send: (object) => {
		return {
		    status: value,
		    object: object,
		}
	    }
	}
    },
}

describe("Response", () => {
    it("error", () => {
	[1, 2, 3].forEach((i) => {
	    let result = Answer.error(res, i)
	    expect(result.status).to.equal(400)
	    expect(result.object).not.to.equal(undefined)
	    expect(result.object.message).to.equal(i)
	})
    })
    
    it("success", () => {
	[1, 2, 3].forEach((i) => {
	    let result = Answer.success(res, i)
	    expect(result.status).to.equal(200)
	    expect(result.object).to.equal(i)
	})

	let result = Answer.success(res, undefined)
	expect(result.status).to.equal(400)
	expect(result.object).not.to.equal(undefined)
	expect(result.object.message).to.equal("Not found")
    })
    
    it("serverError", () => {
	[1, 2, 3].forEach((i) => {
	    let result = Answer.serverError(res, i)
	    expect(result.status).to.equal(500)
	    expect(result.object).not.to.equal(undefined)
	    expect(result.object.message).to.equal(i)
	})
    })
})
