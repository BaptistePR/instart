import polyfill from "babel-polyfill"
import Services from "../../middleWare/GenericServices"
import { expect } from "chai"

let db = {
    find: (e) => {return [1, 2, 3]},
    create: (item) => {return item},
    deleteOne: (item) => {return item},
    findOne: (e) => {return e},
}
let reqUsername = {
    query: {
	username: "pololo",
    }
}

describe("Services", () => {
    it("get all items", async () => {
	let res = await Services.getAllItems(db)
	expect(res).to.deep.equal([1, 2, 3])

	res = await Services.getAllItems(db, {})
	expect(res).to.deep.equal([1, 2, 3])
    })

    it("get one item", async () => {
	let res = await Services.getOneItem(db, 1)
	expect(res).to.equal(1)
    })

    describe("get", () => {
	it("should find", () => {
	    [1, 2, 3].forEach(async (i) => {
		let res = await Services.get({},
					     (item) => { return item === i },
					     db)
		expect(res).to.equal(i)
	    })
	})
	it("should not find", () => {
	    [7, 42, 23, 72].forEach(async (i) => {
		let res = await Services.get({},
					     (item) => { return item === i },
					     db)
		expect(res).to.equal(undefined)
	    })
	})
    })


    describe("get all", () => {
	describe("username / postID", () => {
	    it("should find", async () => {
		let res = await Services.getAll(reqUsername,
						(item) => { return item > 1 },
						db)
		expect(res).to.deep.equal([2, 3])
		
		res = await Services.getAll(reqUsername,
					    (item) => { return item > 0 },
					    db)
		expect(res).to.deep.equal([1, 2, 3])
		
		res = await Services.getAll(reqUsername,
					    (item) => { return item > 2 },
					    db)
		expect(res).to.deep.equal([3])
	    })

	    it("should not find", async () => {
		let res = await Services.getAll(reqUsername,
						(item) => { return item < 1 },
						db)
		expect(res).to.equal(undefined)
	    })
	})

	describe("range", () => {
	    it("should find", async () => {
		let reqRange = {
		    query: {
			start: 1,
			length: 45,
		    }
		}

		let res = await Services.getAll(reqRange,
						()=>{true},
						db)
		expect(res).to.deep.equal([2, 3])

		reqRange.query.start = 0
		res = await Services.getAll(reqRange,
					    ()=>{true},
					    db)
		expect(res).to.deep.equal([1, 2, 3])
	    })

	    it("should not find", async () => {
		let reqRange = {
		    query: {
			start: 28,
			length: 45,
		    }
		}

		let res = await Services.getAll(reqRange,
						()=>{true},
						db)
		expect(res).to.equal(undefined)

		reqRange.query.start = 0
		reqRange.query.length = 0
		res = await Services.getAll(reqRange,
					    ()=>{true},
					    db)
		expect(res).to.equal(undefined)
	    })
	})
    })

    it("add", async () => {
	let isValide = (e) => {return true}
	let isInvalide = (e) => {return false}

	let added = await Services.add(5, isValide, db)
	expect(added).to.equal(5)

	added = await Services.add(5, isInvalide, db)
	expect(added).to.equal(undefined)
    })

    it("remove", async () => {
	let res

	res = await Services.remove(5, db)
	expect(res).to.equal(5)

	res = await Services.remove(false, db)
	expect(res).to.equal(undefined)
    })
})
