import polyfill from "babel-polyfill"
import Security from "../../middleWare/IdentificationTools"
import { expect } from "chai"

let res = {
    status: (value) => {
	return {
	    send: (object) => {
		return {
		    status: value,
		    object: object,
		}
	    }
	}
    },
}

// tests
describe("Identification", () => {
    it("hash password", () => {
	["cuhoe", "fhzoejfziou", "uhcUGfne5ç\"è_'", "loulou"].forEach((s) => {
	    expect(Security.hashPassword(s)).to.not.equal(s)
	})
    })

    it("identify", async () => {
	let isLogged

	isLogged = await Security.identify("pololo", "youpi")
	expect(isLogged).to.be.false

	isLogged = await Security.identify("pololo", "pololo")
	expect(isLogged).to.be.true
    })

    it("create token", () => {
	[undefined, {}, {pololo: "pololo"}].forEach((item) => {
	    let token = Security.createJWToken(item)

	    expect(token).to.be.a("string")
	    expect(!!token.match(/([^\.]+\.){2}[^\.]/g)).to.be.true
	})
    })

    describe("verify JWT token", () => {
	it("should succeed", async () => {
	    let token = Security.createJWToken({})
	    try{
		await Security.verifyJWTToken(token)
	    }catch(err){
		expect(err).not.to.equal(undefined)
		expect(false).to.be.true // because token verification failed but should not
	    }
	})

	it("should fail", async () => {
	    let token = "pololou"
	    try{
		await Security.verifyJWTToken(token)
		expect(false).to.be.true // because token verification succeeded but should not
	    }catch(err){}
	})
    })

    describe("verify JWT_MW token", () => {
	it("should succeed", async () => {
	    let token = Security.createJWToken({})
	    let req = {headers: {authorization: token}}
	    
	    let result = await Security.verifyJWT_MW(req, res, () => { return true })
	    expect(result).to.be.true
	})

	it("should fail", async () => {
	    let token = "pololou"
	    let req = {headers: {authorization: token}}
	    
	    let result = await Security.verifyJWT_MW(req, res, () => {return undefined})
	    expect(result).not.to.equal(undefined)
	    expect(result.object).not.to.equal(undefined)
	    expect(result.object.message).to.equal("Invalid auth token provided.")
	    expect(result.status).to.equal(400)
	})
    })
})
