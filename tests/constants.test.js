import Security from "../middleWare/IdentificationTools"

let Users = [
    {
	username: "pololo",
	password: Security.hashPassword("pololo"),
    },
    {
	username: "oui",
	password: Security.hashPassword("non"),
    },
]

let Posts = [
    {
        username: "pololo",
        title: "a post",
        description: "tututu",
        img: "lion.jpg",
        creationDate: new Date(10000),
    },
    {
        username: "oui",
        title: "a post bis",
        description: "tototo",
        creationDate: new Date(20000),
    },
]

let Coms = [
    {
        username: "oui",
        description: "yes",
        creationDate: new Date(20000),
    },
    {
        username: "oui",
        description: "no",
        creationDate: new Date(10000),
    },
    {
        username: "pololo",
        description: "yes",
        creationDate: new Date(20000),
    },
    {
        username: "pololo",
        description: "no",
        creationDate: new Date(10000),
    },
]

let Likes = [
    {
        username: "oui",
        isOnPost: false,
    },
    {
        username: "oui",
        isOnPost: true,
    },
    {
        username: "pololo",
        isOnPost: false,
    },
    {
        username: "pololo",
        isOnPost: true,
    },
]

export default {
    Users,
    Posts,
    Coms,
    Likes,
}
