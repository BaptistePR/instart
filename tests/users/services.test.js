import polyfill from "babel-polyfill"
import Users from "../../components/users/services"
import Coms from "../../components/coms/services"
import Likes from "../../components/likes/services"
import Posts from "../../components/posts/services"
import { expect } from "chai"
import Constants from "../constants.test"
import utils from "./utils"


// tests
describe("Users::services", () => {
    let req = {
        query: {
            id: ""
        }
    }
    
    it("get all users", async () => {
        let users
        let user

        users = await Users.getAllUsers()
        expect(utils.usersAreAllDifferents(users)).to.be.true
        expect(users.length).to.be.equal(Constants.Users.length)
        for(let i = 0; i < users.length; ++i){
            expect(utils.usersContains(Constants.Users, users[i])).to.be.true
        }

        user = Constants.Users[0]
        users = await Users.getAllUsers(user)
        expect(utils.usersAreAllDifferents(users)).to.be.true
        expect(users.length).to.be.equal(1)
        expect(utils.usersAreEquals(user, users[0])).to.be.true


        user = {username: "bah non", password: "toujours pas"}
        users = await Users.getAllUsers(user)
        expect(users).to.be.deep.equal([])
    })

    it("get one user", async () => {
        let user

        for(let i = 0; i < Constants.Users.length; ++i){
            user = await Users.getOneUser(Constants.Users[i])
            expect(utils.usersAreEquals(user, Constants.Users[i])).to.be.true
        }

        user = await Users.getOneUser({username: "bah non"})
        expect(!user).to.be.true
    })

    it("is valide user", async () => {
        let user = {}
        
        expect(await Users.isValideUser(user)).to.be.false

        user.username = "undefined username"
        expect(await Users.isValideUser(user)).to.be.false

        user.password = "pololo"
        expect(await Users.isValideUser(user)).to.be.true

        user.username = Constants.Users[0].username
        expect(await Users.isValideUser(user)).to.be.false
    })

    it("get", async () => {
        let users = await Users.getAllUsers()
        
        for(let i = 0; i < users.length; ++i){
            req.query.id = users[i]._id
            expect(utils.usersAreEquals(users[i], await Users.get(req))).to.be.true
        }

        req.query.id = "nope"
        expect(!(await Users.get(req))).to.be.true
    })

    it("getAll", async () => {
        let users = await Users.getAllUsers()
        
        for(let i = 0; i < users.length; ++i){
            req.query.username = users[i].username
            let user = await Users.getAll(req)
            expect(user).not.to.be.equal(undefined)
            expect(user.length).to.be.equal(1)
            expect(utils.usersAreEquals(users[i], user[0])).to.be.true
        }

        req.query.username = "undefined username"
        expect(await Users.getAll(req)).to.be.equal(undefined)
    })

    it("add", async () => {
        let dataTest = [
            {
                user: Constants.Users[0],
                result: undefined,
            },
            {
                user: {
                    username: "pololo",
                    password: "pololo",
                },
                result: undefined,
            },
            {
                user: {
                    username: "oui-oui",
                    password: "non-non",
                },
                result: "same",
            },
        ]

        for(let i = 0; i < dataTest.length; ++i){
            let user = dataTest[i].user
            let result = dataTest[i].result

            if(result === "same"){
                expect(user.username).to.be.equal((await Users.add(user)).username)
                let users = await Users.getAllUsers({username: user.username})
                expect(users).not.to.be.equal(undefined)
            }else{
                expect(await Users.add(user)).to.be.equal(result)
            }
        }
    })

    it("remove", async () => {
        let users = await Users.getAllUsers()
        
        expect((await Users.remove({username: "not defined"})).deletedCount).to.be.equal(0)

        for(let i = 0; i < Constants.Users.length; ++i){
            expect((await Users.remove({username: Constants.Users[i].username})).deletedCount).to.be.equal(1)
        }

        let posts = await Posts.getAllPosts()
        let coms = await Coms.getAllComs()
        let likes = await Likes.getAllLikes()
        for(let i = 0; i < users.length; ++i){
            for(let j = 0; j < posts.length; ++j){
                expect(posts[j].username).not.to.be.equal(users[i].username)
            }
            for(let j = 0; j < coms.length; ++j){
                expect(coms[j].username).not.to.be.equal(users[i].username)
            }
            for(let j = 0; j < likes.length; ++j){
                expect(likes[j].username).not.to.be.equal(users[i].username)
            }
        }
    })
})
