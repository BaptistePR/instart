function usersAreEquals(user1, user2){
    return (!user1 && !user2)
        || (user1 && user2 && user1.username == user2.username && user1.password == user2.password)
}

function usersContains(users, user){
    if(users){
        for(let i = 0; i < users.length; ++i){
            if(usersAreEquals(users[i], user)){
                return true
            }
        }
    }

    return false
}

function usersAreAllDifferents(users){
    if(users){
        for(let i = 0; i < users.length - 1; ++i){
            for(let j = i + 1; j < users.length; ++j){
                if(usersAreEquals(users[i], users[j])){
                    return false
                }
            }
        }
    }
    return true
}

export default {
    usersAreEquals,
    usersContains,
    usersAreAllDifferents,
}
