import polyfill from "babel-polyfill"
import User from "../../components/users/controller"
import UserService from "../../components/users/services"
import UserModel from "../../components/users/model"
import { expect } from "chai"
import Constants from "../constants.test"
import utils from "./utils"

let res = {
    status: (value) => {
	return {
	    send: (object) => {
		return {
		    status: value,
		    object: object,
		}
	    }
	}
    },
}

// tests
describe("Users::controller", () => {    
    it("get", async () => {
        let req = {}
        let result
        let users = await UserService.getAllUsers()

        // no query
        req.query = {}
        result = await User.get(req, res)
        expect(result.status).to.be.equal(400)

        // testing id
        req.query = {}
        for(let i = 0; i < users.length; ++i){
            req.query.id = users[i]._id
            result = await User.get(req, res)
            expect(result.status).to.be.equal(200)
            expect(utils.usersAreEquals(users[i], result.object)).to.be.true
        }
        req.query.id = "nop nop nop"
        result = await User.get(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object).to.be.deep.equal([])

        // testing username
        req.query = {}
        for(let i = 0; i < users.length; ++i){
            req.query.username = users[i].username
            result = await User.get(req, res)
            expect(result.status).to.be.equal(200)
            expect(utils.usersAreEquals(users[i], result.object[0])).to.be.true
        }
        req.query.username = "nop nop nop"
        result = await User.get(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object).to.be.deep.equal([])

        // testing range
        req.query = {}
        req.query.start = 0
        req.query.length = users.length
        result = await User.get(req, res)
        expect(result.object).to.be.deep.equal(users)
        
        req.query.start = 0
        req.query.length = users.length - 1
        result = await User.get(req, res)
        expect(result.object).to.be.deep.equal(users.slice(0, req.query.length))
        
        req.query.start = 1
        req.query.length = users.length - 1
        result = await User.get(req, res)
        expect(result.object).to.be.deep.equal(users.slice(1, 1 + req.query.length))

        // testing range & username
        req.query = {}
        req.query.start = 0
        req.query.length = users.length
        for(let i = 0; i < users.length; ++i){
            req.query.username = users[i].username
            result = await User.get(req, res)
            expect(result.status).to.be.equal(200)
            expect(utils.usersAreEquals(users[i], result.object[0])).to.be.true
        }

        // other failing cases
        req.query = {}
        req.query.id = "no"
        req.query.start = "no"
        result = await User.get(req, res)
        expect(result.status).to.be.equal(400)

        req.query.id = undefined
        result = await User.get(req, res)
        expect(result.status).to.be.equal(400)
    })
    
    it("add", async () => {
        let req = {}
        let result
        
        // when it fails
        result = await User.add(req, res)
        expect(result.status).to.be.equal(400)

        // when it succeed
        req.body = {}
        req.body.username = "oui-non"
        req.body.password = "pololo"
        result = await User.add(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object.username).to.be.equal(req.body.username)
    })
    
    it("remove", async () => {
        let req = {body: {username: "not found"}}
        let result

        // when it fails
        result = await User.remove(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object.deletedCount).to.be.equal(0)

        // when it succeed
        let users = await UserService.getAllUsers()
        req.body.username = users[0].username
        result = await User.remove(req, res) 
        expect(result.status).to.be.equal(200)
        expect(result.object.deletedCount).to.be.equal(1)
    })
})
