import _ from "babel-polyfill"
import mongoose from "mongoose"
import { Mockgoose } from "mockgoose"
import { MONGO } from "../conf/env"
import UserModel from "../components/users/model"
import PostModel from "../components/posts/model"
import ComModel from "../components/coms/model"
import LikeModel from "../components/likes/model"
import Constants from "./constants.test"

let mockgoose = new Mockgoose(mongoose)

// mocking db
before((done) => {
    mockgoose.prepareStorage().then(
        () => {
            console.log("Mocked : " + mockgoose.helper.isMocked())
            mongoose.connect(
	        MONGO,
	        { useNewUrlParser: true }
            )
            mongoose.set("useCreateIndex", true)

            let db = mongoose.connection

            db.on("error", (err) => {done(err)})
            db.once("open", async () => {
                done()
            })
        })
})

// closing db
after(async () => {
    await mockgoose.helper.reset()
    await mongoose.disconnect()
    await mockgoose.shutdown()
})

// init db
beforeEach(async () => {
    // users initialization
    for(let i = 0; i < Constants.Users.length; ++i){
	await UserModel.create(Constants.Users[i])
    }
    
    // posts initialization
    for(let i = 0; i < Constants.Posts.length; ++i){
        Constants.Posts[i].likes = 0
        Constants.Posts[i].coms = 0
	await PostModel.create(Constants.Posts[i])
    }
    
    // coms initialization
    let posts = await PostModel.find({})
    for(let i = 0; i < Constants.Coms.length && i / 2 < posts.length; ++i){
        let post = posts[Math.floor(i / 2)]
        
        Constants.Coms[i].postID = post._id.toString()
        Constants.Coms[i].likes = 0
        
	await ComModel.create(Constants.Coms[i])
        try{
            post.coms += 1
            await post.save()
        }catch(err){
            console.log(err)
        }
    }
    
    // likes initialization
    let coms = await ComModel.find({})
    for(let i = 0; i < Constants.Likes.length; ++i){
        let owner
        if(Constants.Likes[i].isOnPost){
            owner = posts[Math.floor(i / 2)]
        }else{
            owner = coms[i]
        }
        Constants.Likes[i].ownerID = owner._id
	await LikeModel.create(Constants.Likes[i])
        try{
            owner.likes += 1
            await owner.save()
        }catch(err){
            console.log(err)
        }
    }

    for(let i = 0; i < Constants.Posts.length; ++i){
        delete Constants.Posts[i].likes
        delete Constants.Posts[i].coms
    }

    for(let i = 0; i < Constants.Coms.length; ++i){
        delete Constants.Coms[i].likes
    }
})

// reset db
afterEach(async () => {
    await mockgoose.helper.reset()
})
    
