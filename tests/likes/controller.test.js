import polyfill from "babel-polyfill"
import Likes from "../../components/likes/controller"
import LikeService from "../../components/likes/services"
import Posts from "../../components/posts/services"
import LikeModel from "../../components/likes/model"
import { expect } from "chai"
import Constants from "../constants.test"
import utils from "./utils"

let res = {
    status: (value) => {
	return {
	    send: (object) => {
		return {
		    status: value,
		    object: object,
		}
	    }
	}
    },
}

// tests
describe("Likes::controller", () => {    
    it("get", async () => {
        let req = {query:{}}
        let likes = await LikeService.getAllLikes()
        let result

        result = await Likes.get(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object).to.be.deep.equal(likes)

        req.query.ownerID = likes[0].ownerID
        result = await Likes.get(req, res)
        expect(result.status).to.be.equal(200)
        let expectedLikes = []
        for(let i = 0; i < likes.length; ++i){
            if(likes[i].ownerID === req.query.ownerID){
                expectedLikes.push(likes[i])
            }
        }
        expect(result.object).to.be.deep.equal(expectedLikes)

        req.query.username = likes[0].username
        result = await Likes.get(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object).to.be.deep.equal([likes[0]])
    })
    
    it("add", async () => {
        let req = {}
        let result
        
        // when it fails
        req.body = {}
        req.body.ownerID = (await LikeService.getAllLikes())[0]._id
        result = await Likes.add(req, res)
        expect(result.status).to.be.equal(400)

        // when it succeed
        req.body = {}
        req.body.username = "oui"
        req.body.ownerID = (await Posts.getAllPosts())[0]._id
        req.body.isOnPost = true
        result = await Likes.add(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object.username).to.be.equal(req.body.username)
    })
    
    it("remove", async () => {
        let req = {body: {username: "not found"}}
        let result

        // when it fails
        result = await Likes.remove(req, res)
        expect(result.status).to.be.equal(400)

        // when it succeed
        let likes = await LikeService.getAllLikes()
        req.body.username = likes[0].username
        result = await Likes.remove(req, res) 
        expect(result.status).to.be.equal(200)
        expect(result.object.deletedCount).to.be.equal(2)
    })
})
