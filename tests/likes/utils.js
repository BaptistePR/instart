function likesAreEquals(like1, like2){
    return (!like1 && !like2)
        || (like1 && like2
            && like1.isOnPost == like2.isOnPost
            && like1.username == like2.username)
}

function likesContains(likes, like){
    if(likes){
        for(let i = 0; i < likes.length; ++i){
            if(likesAreEquals(likes[i], like)){
                return true
            }
        }
    }

    return false
}

function likesAreAllDifferents(likes){
    if(likes){
        for(let i = 0; i < likes.length - 1; ++i){
            for(let j = i + 1; j < likes.length; ++j){
                if(likesAreEquals(likes[i], likes[j])){
                    return false
                }
            }
        }
    }
    return true
}

export default {
    likesAreEquals,
    likesContains,
    likesAreAllDifferents,
}
