import polyfill from "babel-polyfill"
import Likes from "../../components/likes/services"
import Posts from "../../components/posts/services"
import Coms from "../../components/coms/services"
import { expect } from "chai"
import Constants from "../constants.test"
import utils from "./utils"


// tests
describe("Likes::services", () => {
    let req = {
        query: {
            id: ""
        }
    }
    
    it("get all likes", async () => {
        let likes
        let like

        likes = await Likes.getAllLikes()
        expect(utils.likesAreAllDifferents(likes)).to.be.true
        expect(likes.length).to.be.equal(Constants.Likes.length)
        for(let i = 0; i < likes.length; ++i){
            expect(utils.likesContains(Constants.Likes, likes[i])).to.be.true
        }

        like = Constants.Likes[0]
        likes = await Likes.getAllLikes(like)
        expect(utils.likesAreAllDifferents(likes)).to.be.true
        expect(likes.length).to.be.equal(1)
        expect(utils.likesAreEquals(like, likes[0])).to.be.true


        like = {username: "bah non"}
        likes = await Likes.getAllLikes(like)
        expect(likes).to.be.deep.equal([])
    })

    it("get one like", async () => {
        let like

        for(let i = 0; i < Constants.Likes.length; ++i){
            like = await Likes.getOneLike(Constants.Likes[i])
            expect(utils.likesAreEquals(like, Constants.Likes[i])).to.be.true
        }

        like = await Likes.getOneLike({username: "bah non"})
        expect(!like).to.be.true
    })

    it("is valide like", async () => {
        let like = {}
        let posts = await Posts.getAllPosts()
        
        expect(await Likes.isValideLike(like)).to.be.false

        like.username = "pololo"
        expect(await Likes.isValideLike(like)).to.be.false

        like.isOnPost = true
        expect(await Likes.isValideLike(like)).to.be.false

        like.ownerID = posts[0]._id.toString()
        expect(await Likes.isValideLike(like)).to.be.true

        like.username = "beeeee"
        expect(await Likes.isValideLike(like)).to.be.false

        like.username = "pololo"
        like.isOnPost = false
        expect(await Likes.isValideLike(like)).to.be.false
    })

    it("add", async () => {
        let dataTest = [
            {
                like: {
                    username: "pololo",
                },
                result: undefined,
            },
            {
                like: Constants.Likes[0],
                result: "same",
            },
            {
                like: Constants.Likes[1],
                result: "same",
            },
        ]

        for(let i = 0; i < dataTest.length; ++i){
            let like = dataTest[i].like
            let result = dataTest[i].result

            if(result === "same"){
                expect(like.username).to.be.equal((await Likes.add(like)).username)
                let likes = await Likes.getAllLikes({username: like.username})
                expect(likes).not.to.be.equal(undefined)
            }else{
                expect(await Likes.add(like)).to.be.equal(result)
            }
        }
    })

    it("remove", async () => {        
        expect((await Likes.remove({username: "not defined"})).deletedCount).to.be.equal(0)
        for(let i = 0; i < Constants.Likes.length; i += 2){
            console.log(Constants.Likes[i].username)
            expect((await Likes.remove({username: Constants.Likes[i].username})).deletedCount).to.be.equal(2)
        }

        let posts = await Posts.getAllPosts()
        for(let i = 0; i < posts.length; ++i){
            expect(posts[i].likes).to.be.equal(0)
        }

        let coms = await Coms.getAllComs()
        for(let i = 0; i < coms.length; ++i){
            console.log(coms[i].likes)
            expect(coms[i].likes).to.be.equal(0)
        }
    })

})
