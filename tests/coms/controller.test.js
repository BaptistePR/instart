import polyfill from "babel-polyfill"
import Com from "../../components/coms/controller"
import ComService from "../../components/coms/services"
import Posts from "../../components/posts/services"
import ComModel from "../../components/coms/model"
import { expect } from "chai"
import Constants from "../constants.test"
import utils from "./utils"

let res = {
    status: (value) => {
	return {
	    send: (object) => {
		return {
		    status: value,
		    object: object,
		}
	    }
	}
    },
}

// tests
describe("Coms::controller", () => {    
    it("get", async () => {
        let req = {}
        let result
        let coms = await ComService.getAllComs()

        // no query
        req.query = {}
        result = await Com.get(req, res)
        expect(result.status).to.be.equal(400)

        // testing id
        req.query = {}
        for(let i = 0; i < coms.length; ++i){
            req.query.id = coms[i]._id
            result = await Com.get(req, res)
            expect(result.status).to.be.equal(200)
            expect(utils.comsAreEquals(coms[i], result.object)).to.be.true
        }
        req.query.id = "nop nop nop"
        result = await Com.get(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object).to.be.deep.equal([])

        // testing username
        req.query = {}
        for(let i = 0; i < coms.length; i += 2){
            req.query.username = coms[i].username
            result = await Com.get(req, res)
            expect(result.status).to.be.equal(200)
            expect(result.object.length).to.be.equal(2)
            expect(utils.comsAreEquals(coms[i], result.object[0])).to.be.true
            expect(utils.comsAreEquals(coms[i + 1], result.object[1])).to.be.true
        }
        req.query.username = "nop nop nop"
        result = await Com.get(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object).to.be.deep.equal([])

        // testing range
        req.query = {}
        req.query.start = 0
        req.query.length = coms.length
        result = await Com.get(req, res)
        expect(result.object).to.be.deep.equal(coms)
        
        req.query.start = 0
        req.query.length = coms.length - 1
        result = await Com.get(req, res)
        expect(result.object).to.be.deep.equal(coms.slice(0, req.query.length))
        
        req.query.start = 1
        req.query.length = coms.length - 1
        result = await Com.get(req, res)
        expect(result.object).to.be.deep.equal(coms.slice(1, 1 + req.query.length))

        // testing range & username
        req.query = {}
        req.query.start = 0
        req.query.length = 1
        for(let i = 0; i < coms.length; ++i){
            req.query.username = coms[i].username
            result = await Com.get(req, res)
            expect(result.status).to.be.equal(200)
            expect(utils.comsAreEquals(coms[Math.floor(i / 2) * 2], result.object[0])).to.be.true
        }

        // testing postID
        let posts = await Posts.getAllPosts()
        req.query = {}
        for(let i = 0; i < coms.length; i += 2){
            req.query.postID = posts[Math.floor(i / 2)]._id.toString()
            result = await Com.get(req, res)
            expect(result.status).to.be.equal(200)
            expect(result.object.length).to.be.equal(2)
            expect(utils.comsAreEquals(coms[i], result.object[0])).to.be.true
            expect(utils.comsAreEquals(coms[i + 1], result.object[1])).to.be.true
        }

        // other failing cases
        req.query = {}
        req.query.id = "no"
        req.query.start = "no"
        result = await Com.get(req, res)
        expect(result.status).to.be.equal(400)

        req.query.id = undefined
        result = await Com.get(req, res)
        expect(result.status).to.be.equal(400)

        req.query = {}
        req.query.id = 1
        req.query.postID = 1
        result = await Com.get(req, res)
        expect(result.status).to.be.equal(400)
    })
    
    it("add", async () => {
        let req = {}
        let result
        
        // when it fails
        result = await Com.add(req, res)
        expect(result.status).to.be.equal(400)

        // when it succeed
        req.body = {}
        req.body.username = "oui"
        req.body.postID = (await Posts.getAllPosts())[0]._id
        req.body.creationDate = new Date()
        req.body.description = ""
        result = await Com.add(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object.username).to.be.equal(req.body.username)
    })
    
    it("remove", async () => {
        let req = {body: {username: "not found"}}
        let result

        // when it fails
        result = await Com.remove(req, res)
        expect(result.status).to.be.equal(200)
        expect(result.object.deletedCount).to.be.equal(0)

        // when it succeed
        let coms = await ComService.getAllComs()
        req.body.username = coms[0].username
        result = await Com.remove(req, res) 
        expect(result.status).to.be.equal(200)
        expect(result.object.deletedCount).to.be.equal(2)
    })
})
