import polyfill from "babel-polyfill"
import Coms from "../../components/coms/services"
import Posts from "../../components/posts/services"
import { expect } from "chai"
import Constants from "../constants.test"
import utils from "./utils"


// tests
describe("Coms::services", () => {
    let req = {
        query: {
            id: ""
        }
    }
    
    it("get all coms", async () => {
        let coms
        let com

        coms = await Coms.getAllComs()
        expect(utils.comsAreAllDifferents(coms)).to.be.true
        expect(coms.length).to.be.equal(Constants.Coms.length)
        for(let i = 0; i < coms.length; ++i){
            expect(utils.comsContains(Constants.Coms, coms[i])).to.be.true
        }

        com = Constants.Coms[0]
        coms = await Coms.getAllComs(com)
        expect(utils.comsAreAllDifferents(coms)).to.be.true
        expect(coms.length).to.be.equal(1)
        expect(utils.comsAreEquals(com, coms[0])).to.be.true


        com = {username: "bah non", password: "toujours pas"}
        coms = await Coms.getAllComs(com)
        expect(coms).to.be.deep.equal([])
    })

    it("get one com", async () => {
        let com

        for(let i = 0; i < Constants.Coms.length; ++i){
            com = await Coms.getOneCom(Constants.Coms[i])
            expect(utils.comsAreEquals(com, Constants.Coms[i])).to.be.true
        }

        com = await Coms.getOneCom({username: "bah non"})
        expect(!com).to.be.true
    })

    it("is valide com", async () => {
        let posts = await Posts.getAllPosts()
        let com = {}
        
        expect(await Coms.isValideCom(com)).to.be.false

        com.postID = posts[0]._id
        expect(await Coms.isValideCom(com)).to.be.false

        com.description = ""
        expect(await Coms.isValideCom(com)).to.be.false

        com.creationDate = new Date()
        expect(await Coms.isValideCom(com)).to.be.false

        com.username = "undefined username"
        expect(await Coms.isValideCom(com)).to.be.false

        com.username = Constants.Users[0].username
        expect(await Coms.isValideCom(com)).to.be.true

        com.postID = Constants.Users[0]._id
        expect(await Coms.isValideCom(com)).to.be.false
    })

    it("get", async () => {
        let coms = await Coms.getAllComs()
        
        for(let i = 0; i < coms.length; ++i){
            req.query.id = coms[i]._id
            expect(utils.comsAreEquals(coms[i], await Coms.get(req))).to.be.true
        }

        req.query.id = "nope"
        expect(!(await Coms.get(req))).to.be.true
    })

    it("getAll", async () => {
        let coms = await Coms.getAllComs()
        
        for(let i = 0; i < coms.length; i += 2){
            req.query.username = coms[i].username
            let com = await Coms.getAll(req)
            expect(com).not.to.be.equal(undefined)
            expect(com.length).to.be.equal(2)
            expect(utils.comsAreEquals(coms[i], com[0])).to.be.true
            expect(utils.comsAreEquals(coms[i + 1], com[1])).to.be.true
        }

        req.query.username = "undefined username"
        expect(await Coms.getAll(req)).to.be.equal(undefined)
    })

    it("add", async () => {
        let dataTest = [
            {
                com: {
                    username: "pololo",
                    password: "pololo",
                },
                result: undefined,
            },
            {
                com: Constants.Coms[0],
                result: "same",
            },
        ]

        for(let i = 0; i < dataTest.length; ++i){
            let com = dataTest[i].com
            let result = dataTest[i].result

            if(result === "same"){
                expect(com.username).to.be.equal((await Coms.add(com)).username)
                let coms = await Coms.getAllComs({username: com.username})
                expect(coms).not.to.be.equal(undefined)
            }else{
                expect(await Coms.add(com)).to.be.equal(result)
            }
        }
    })

    it("remove", async () => {
        expect((await Coms.remove({username: "not defined"})).deletedCount).to.be.equal(0)
        expect((await Coms.remove({username: Constants.Coms[0].username})).deletedCount).to.be.equal(2)
    })

    it("set property", async () => {
        let coms = await Coms.getAllComs()
        let id = coms[0]._id
        let description = coms[0].description

        expect(await Coms.setProperty(Constants.Users[0]._id, "username", (username) => {return username + "1"})).to.be.equal(3)
        expect(await Coms.setProperty(id, "pololo", (username) => {return username + "1"})).to.be.equal(2)
        expect(await Coms.setProperty(id, "description", (c) => {return c + "1"})).to.be.equal(1)

        let com = await Coms.getOneCom({_id: id})
        expect(com.description).to.be.equal(description + "1")
    })

    it("dec likes", async () => {
        let coms = await Coms.getAllComs()
        let id = coms[0]._id
        let likes = coms[0].likes
        
        expect(await Coms.decLikes(Constants.Users[0]._id)).to.be.equal(3)
        expect(await Coms.decLikes(id)).to.be.equal(1)

        let com = await Coms.getOneCom({_id: id})
        expect(com.likes).to.be.equal(likes - 1)
    })

    it("inc likes", async () => {
        let coms = await Coms.getAllComs()
        let id = coms[0]._id
        let likes = coms[0].likes
        
        expect(await Coms.incLikes(Constants.Users[0]._id)).to.be.equal(3)
        expect(await Coms.incLikes(id)).to.be.equal(1)

        let com = await Coms.getOneCom({_id: id})
        expect(com.likes).to.be.equal(likes + 1)
    })
})
