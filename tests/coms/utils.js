function comsAreEquals(com1, com2){
    return (!com1 && !com2)
        || (com1 && com2 && com1.username == com2.username
            && com1.creationDate.getTime() == com2.creationDate.getTime()
            && com1.description == com2.description)
}

function comsContains(coms, com){
    if(coms){
        for(let i = 0; i < coms.length; ++i){
            if(comsAreEquals(coms[i], com)){
                return true
            }
        }
    }

    return false
}

function comsAreAllDifferents(coms){
    if(coms){
        for(let i = 0; i < coms.length - 1; ++i){
            for(let j = i + 1; j < coms.length; ++j){
                if(comsAreEquals(coms[i], coms[j])){
                    return false
                }
            }
        }
    }
    return true
}

export default {
    comsAreEquals,
    comsContains,
    comsAreAllDifferents,
}
