async function getAllItems(db, filter){
    if(filter){
        return db.find(filter)
    }else{
        return db.find({})
    }
}

async function getOneItem(db, filter){
    return db.findOne(filter)
}

async function get(req, isSearched, db){
    let items = await getAllItems(db)

    return items.find((item) => {
        return isSearched(item)
    })
}

async function getAll(req, isSearched, db, behavior){
    let items = await getAllItems(db)

    if(items.length > 0 && behavior){
        items = behavior(items)
    }

    if(req.query.username || req.query.postID){
        let tmpItems = []
        items.map((item) => {
            if(isSearched(item)){
                tmpItems.push(item)
            }
        })
        items = tmpItems
    }

    if(req.query.start !== undefined && req.query.start !== null){
        const start = parseInt(req.query.start, 10)
        const length = parseInt(req.query.length, 10)

        items = items.slice(start, start + length)
    }
    
    if(items.length == 0 || req.query.length === 0){
        items = undefined
    }

    return items
}

async function add(item, isValide, db){
    let valide = await isValide(item)
    if(valide){
        return db.create(item)
    }
    return undefined
}

async function remove(filter, db){
    if(filter){
        return await db.deleteOne(filter)
    }
    return undefined
}

export default {
    get,
    getAll,
    add,
    remove,
    getAllItems,
    getOneItem,
}
