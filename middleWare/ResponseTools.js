function error(res, message){
    return res.status(400).send({
        message: message,
    })
}

function success(res, object){
    if(!object){
        return error(res, "Not found")
    }
    return res.status(200).send(object)
}

function serverError(res, message){
    return res.status(500).send({
        message: message,
    })
}
export default {
    error,
    success,
    serverError,
}
