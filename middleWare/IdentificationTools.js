import jwt from "jsonwebtoken"
import lodash from "lodash"
import crypto from "crypto"
import Users from "../components/users/model"
import Services from "./GenericServices"
import Answer from "./ResponseTools"
import { JWT_SECRET } from "../conf/env"

async function verifyJWTToken(token) {
    try{
        const decodedToken = await jwt.verify(token, JWT_SECRET)

        if(!decodedToken){
            throw "error"
        }
	
        return decodedToken
    }catch(err){
        throw "error"
    }
}

function createJWToken(details){
    if(typeof details !== "object"){
        details = {}
    }

    if(!details.iat || typeof details.iat !== "number"){
        details.iat = 3600
    }

    details.sessionData = lodash.reduce(
        details.sessionData || {},
        (memo, val, key) => {
            if (typeof val !== "function" && key !== "password") {
                memo[key] = val
            }
            return memo
        },
        {})

    let token = jwt.sign(
        {
            data: details.sessionData
        },
        JWT_SECRET,
        {
            expiresIn: details.iat,
            algorithm: "HS256"
        })

    return token
}

async function verifyJWT_MW(req, res, next){
    let token = req.headers.authorization

    try{
        let decodedToken = await verifyJWTToken(token)

        if(!decodedToken){
            throw "error"
        }

        req.user = decodedToken.data
        console.log("IdentificationTools::verifyJWT_MW: could log in")
        return next()
    }catch(err){
        console.log("IdentificationTools::verifyJWT_MW: could not log in")
        return Answer.error(res, "Invalid auth token provided.")
    }
}

function hashPassword(password){
    return crypto.createHash("sha256").update(password).digest("base64")
}

async function identify(username, password){
    let users = await Services.getAllItems(Users)
    let hashed = hashPassword(password)

    return !!(users.find((user) => {
        return user.username === username && user.password === hashed
    }))
}

export default {
    verifyJWTToken,
    createJWToken,
    verifyJWT_MW,
    hashPassword,
    identify,
}
