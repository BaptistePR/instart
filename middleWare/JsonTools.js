function htmlToJson(html){
    if(html){
        let result = {}
        let values = html.split("&")

        for(let i = 0; i < values.length; ++i){
            let pair = values[i].split("=")
            result[pair[0]] = pair[1]
        }
	
        return result
    }
    return undefined
}

export default {
    htmlToJson,
}
