function applyRule(list, fun){
    let i = 0
    let res = true

    while(res && i < list.length){
        res = fun(list[i])
        ++i
    }

    return res
}
    

function areUndefined(list){
    return applyRule(list, (item) => item === undefined)
}

function areDefined(list){
    return applyRule(list, (item) => item !== undefined)
}
    
export default {
    applyRule,
    areUndefined,
    areDefined,
}
