import Likes from "./controller.js"
import express from "express"

const routes = express.Router()
const LIKES_ROUTE = "/likes"

// get likes
routes.route(LIKES_ROUTE).get(Likes.get)
// post likes
routes.route(LIKES_ROUTE).post(Likes.add)
// delete likes
routes.route(LIKES_ROUTE).delete(Likes.remove)

export default {
    routes,
    LIKES_ROUTE,
}
