import Mongoose from "mongoose"

const Schema = Mongoose.Schema

var LikesSchema = new Schema({
    ownerID: String,
    isOnPost: Boolean,
    username: String,
})

LikesSchema.index({ ownerID: 1})
let Likes = Mongoose.model("Likes", LikesSchema)

export default Likes
