import db from "./model"
import Coms from "../coms/services"
import Posts from "../posts/services"
import Users from "../users/services"
import Services from "../../middleWare/GenericServices"
import Query from "../../middleWare/QueryTools"

async function getAllLikes(filter) {
    console.log("likes::services::getAllLikes")
    return Services.getAllItems(db, filter)
}

async function getOneLike(filter) {
    console.log("likes::services::getOneLike")
    return Services.getOneItem(db, filter)
}

async function isValideLike(like) {
    console.log("likes::services::isValideLike")
    if (like && Query.areDefined([like.ownerID, like.username, like.isOnPost])) {
        let owner = undefined
        if(like.isOnPost){
            owner = await Posts.getOnePost({_id: like.ownerID})
        }else{
            owner = await Coms.getOneCom({_id: like.ownerID})
        }

        if (!owner) {
            console.log("owner not found")
            return false
        }

        let user = await Users.getOneUser({username: like.username})
        if (!user) {
            console.log("user not found")
            return false
        }

        return true
    }else{
        console.log("like empty or laking content")
    }

    return false
}

async function add(like) {
    console.log("likes::services::add")
    if (like.ownerID) {
        if(like.isOnPost){
            await Posts.incLikes(like.ownerID)
        }else{
            await Coms.incLikes(like.ownerID)
        }
    }
    return await Services.add(like,
        isValideLike,
        db)
}

async function getAllByOwnerId(req) {
    console.log("likes::services::getAllByOwnerId")
    return await Services.getAll(req,
        (like) => {
            return like.ownerID === req.query.ownerID
        },
        db)
}

async function remove(filter) {
    console.log("likes::services::removeFrom")
    let likes = await getAllLikes(filter)

    if (likes && likes.length > 0) {
        for(let i = 0; i < likes.length; ++i){
            let item = likes[i]
            if(item.isOnPost){
                await Posts.decLikes(item.ownerID)
            }else{
                await Coms.decLikes(item.ownerID)
            }
            await db.deleteOne({_id: item._id})
        }
    } else {
        console.log("no like found")
    }

    return {deletedCount: likes.length}
}

export default {
    add,
    remove,
    getAllByOwnerId,
    getAllLikes,
    getOneLike,
    isValideLike,
}
