import Likes from "./services"
import Answer from "../../middleWare/ResponseTools.js"

async function get(req, res){
    console.log("likes::controller::get")
    let filter = {}

    if(req.query.ownerID){
        filter.ownerID = req.query.ownerID
	
        if(req.query.username){
            filter.username = req.query.username
        }
    }

    let likes = await Likes.getAllLikes(filter)
    if(!likes){
        likes = []
    }

    return Answer.success(res, likes)
}

async function add(req, res){
    console.log("likes::controller::add")
    let result = await Likes.add(req.body)

    if(result !== undefined){
        return Answer.success(res, result)
    }else{
        return Answer.error(res, "Likes could not be added")
    }
}

async function remove(req, res){
    console.log("likes::controller::remove")
    let result = await Likes.remove(req.body)

    if(result.deletedCount){
        return Answer.success(res, result)
    }else{
        return Answer.error(res, "failed to remove")
    }
}

export default {
    get,
    add,
    remove
}
