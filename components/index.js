import express from "express"
import post from "./posts/routes"
import user from "./users/routes"
import coms from "./coms/routes"
import likes from "./likes/routes"
import Security from "../middleWare/IdentificationTools"

const API_NAME = "/instazz/1.0.0"

const routes = express.Router()

routes.all(API_NAME + post.POST_ROUTE + "/*", Security.verifyJWT_MW)
routes.all(API_NAME + coms.COMS_ROUTE + "/*", Security.verifyJWT_MW)
routes.all(API_NAME + likes.LIKE_ROUTE + "/*", Security.verifyJWT_MW)

routes.use(API_NAME, post.routes)
routes.use(API_NAME, user.routes)
routes.use(API_NAME, coms.routes)
routes.use(API_NAME, likes.routes)

// Security
routes.post(API_NAME + "/login", async (req, res) => {
    let { username, password } = req.body
    
    let valid = await Security.identify(username, password)

    if(valid){
        res.status(200).json({
            success: true,
            token: Security.createJWToken({
                sessionData: { username: username },
                iat: 3600,
            })
        })
    }else{
        res.status(401).json({
            message: "Login ou mot de passe incorrecte."
        })
    }
})

export default routes
