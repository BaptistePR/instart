import Mongoose from "mongoose"

const Schema = Mongoose.Schema

var PostSchema = new Schema({
    username: String,
    title: String,
    description: String,
    img: String,
    coms: Number,
    likes: Number,
    creationDate: Date,
})

PostSchema.index({creationDate: 1, username: 2})
let Post = Mongoose.model("Post", PostSchema)

export default Post
