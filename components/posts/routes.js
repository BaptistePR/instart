import Post from "./controller"
import express from "express"
import multer from "multer"

const routes = express.Router()
const POST_ROUTE = "/posts"

var upload = multer({ dest: "resources" })

// get post
routes.route(POST_ROUTE).get(Post.get)
// get post image
routes.route(POST_ROUTE + "/upload").get(Post.getImage)
// post post
routes.route(POST_ROUTE).post(upload.single("file"), Post.add)
// delete post
routes.route(POST_ROUTE).delete(Post.remove)

export default {
    routes,
    POST_ROUTE,
}
