import Post from "./services"
import Services from "../../middleWare/GenericServices"
import Answer from "../../middleWare/ResponseTools"
import Query from "../../middleWare/QueryTools"
import db from "./model"
import fs from "fs"
import path from "path"

async function get(req, res) {
    console.log("posts::controller::get")
    let result = undefined
    let fun = undefined

    if (req.query.id === undefined) {
        if (((req.query.start === undefined) + (req.query.length === undefined) == 1)) {
            result = Answer.error(res, "If start or length is defined, the other needs to be")
        } else if (Query.areUndefined([req.query.start, req.query.length, req.query.username])) {
            result = Answer.error(res, "Needs parameters defined : id OR (username AND/OR (start AND length))")
        } else {
            fun = Post.getAll
        }
    } else {
        console.log(req.query.id)
        if (Query.areUndefined([req.query.start, req.query.length, req.query.username])) {
            fun = Post.get
        } else {
            result = Answer.error(res, "If id query is defined, no other should be")
        }
    }

    if (fun !== undefined) {
        let posts = await fun(req, res)
        if (posts === undefined) {
            result = Answer.success(res, []) // item not found
        } else {
            result = Answer.success(res, posts)
        }
    }

    if (result === undefined) {
        result = Answer.error(res, "Needs parameters defined : id OR (username AND/OR (start AND length))")
    }

    return result
}

async function getImage(req, res) {
    console.log("posts::controller::getImage")
    if (!req.query.id) {
        return Answer.error(res, "id undefined")
    }

    let post = await Services.get(
        req,
        (post) => {
            return post && post._id.equals(req.query.id)
        },
        db)
    if (post && post.img) {
        return await Post.loadImage(res, post)
    } else if (post) {
        console.log(post)
        return Answer.error(res, "post do not have image")
    } else {
        return Answer.success(res, []) // "post not found"
    }
}

async function add(req, res) {
    console.log("posts::controller::add")
    if (!req.query.id) {
        let result = await Post.add(req.body)

        if (result) {
            return Answer.success(res, result)
        } else {
            console.log(req.body)
            return Answer.error(res, "invalid post")
        }
    } else if (req.file) {
        let unlink = (file, sendRes) => {
            fs.unlink(file, err => {
                if (err) {
                    console.log(err)
                    if (sendRes) {
                        return Answer.serverError(res, "The file linked to the post needs to be a jpg file")
                    }
                }
            })
        }
        
        let result = await Post.getOnePost({_id: req.query.id})

        if (!result) {
            return Answer.success(res, [])
        }

        if(result.img){
            unlink("resources/" + result.img, false)
        }

        result.img = req.file.filename + ".jpg"
        try{
            await result.save()
        }catch(err){
            console.log("Failed to update post on creation : ")
            console.log(err)
        }
        
        if (path.extname(req.file.originalname).toLowerCase() === ".jpg") {
            fs.rename(req.file.path, "resources/" + result.img, err => {
                if (err) {
                    Post.remove(result)
                    unlink(req.file.path, false)
                    console.log("could not rename")
                    console.log(err)
                    return Answer.serverError(res, "Failed to save the post on the server")
                } else {
                    return Answer.success(res, result)
                }
            })
        } else {
            let res = unlink(req.file.path, true)
            if(res) return res
            return Answer.error(res, "The file specified needs to be of format jpg")
        }
    } else {
        return Answer.error(res, "Post could not be added - no image")
    }
}

async function remove(req, res) {
    console.log("posts::controller::remove")
    let result = await Post.getOnePost({_id: req.body._id})
    
    if (result) {
        if (result.img && result.img != "") {
            fs.unlink("resources/" + result.img, err => {
                if (err) {
                    console.log(err)
                }
            })
        }

        result = await Post.remove(result)

        if (result) {
            return Answer.success(res, result)
        } else {
            return Answer.serverError(res, "failed to remove")
        }
    }else{
        return Answer.error(res, "Not found")
    }
}

export default {
    get,
    add,
    remove,
    getImage,
}
