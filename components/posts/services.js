import db from "./model"
import Services from "../../middleWare/GenericServices"
import Users from "../users/services"
import Likes from "../likes/services"
import Coms from "../coms/services"
import Query from "../../middleWare/QueryTools"

async function getAllPosts(filter){
    console.log("posts::services::getAllPosts")
    return (await Services.getAllItems(db, filter))
}

async function getOnePost(filter){
    console.log("posts::services::getOnePost")
    return Services.getOneItem(db, filter)
}

async function isValidePost(post){
    console.log("posts::services::isValidePost")
    if(post && Query.areDefined([post.username, post.title, post.creationDate])){
        // does the user exist ?
        let owner = await Users.getOneUser({username: post.username})
        if(!owner){
            console.log("owner not found")
            return false
        }

        // set optional parameters
        post.likes = 0
        post.coms = 0

        if(!(post.description)){
            post.description = ""
        }

        return true
    }else{
        console.log("post or title undefined")
    }
    
    return false
}

async function loadImage(res, post){
    console.log("posts::services::loadImage")
    return res.download("resources/" + post.img)
}

async function get(req){
    console.log("posts::services::get")
    return await Services.get(
        req,
        (post) => {return post._id.equals(req.query.id)},
        db)
}

async function getAll(req){
    console.log("posts::services::getAll")
    return await Services.getAll(
        req,
        (post) => {return post.username === req.query.username},
        db,
        (res) => {return res.sort((d1, d2) => {return d2.creationDate > d1.creationDate})}
    )
}

async function add(post){
    console.log("posts::services::add")
    let result = await Services.add(post, isValidePost, db)
    
    return result
    
}

async function remove(filter){
    console.log("posts::services::remove")
    let posts = await getAllPosts(filter)

    if (posts.length > 0) {
        for(let i = 0; i < posts.length; ++i){
            let post = posts[i]
            await Likes.remove({ownerID: post._id, isOnPost:true})
            await Coms.remove({postID: post._id})
            await db.deleteOne({_id: post._id})
        }
    }else{
        console.log("No posts to delete")
    }
    return {deletedCount: posts.length}
}

async function setProperty(postID, property, behave){
    // its returns are for the UTs
    console.log("posts::services::setProperty")
    let post = await getOnePost({_id: postID})

    if(post){
        if(post[property] !== undefined && post[property] !== null){
            post[property] = behave(post[property])
            try {
                await post.save()
            } catch (err) {
                console.log("err: " + err)
            }
            return 1
        }else{
            console.log("unknown property")
            return 2
        }
    }else{
        console.log("post not found")
        return 3
    }
}
    

async function decComs(postID){
    console.log("posts::services::decComs")
    return setProperty(postID, "coms", (i) => {return i - 1})
}

async function decLikes(postID){
    console.log("posts::services::decComs")
    return setProperty(postID, "likes", (i) => {return i - 1})
}

async function incLikes(postID){
    console.log("posts::services::incLikes")
    return setProperty(postID, "likes", (i) => {return i + 1})
}
    

export default {
    get,
    getAll,
    add,
    remove,
    loadImage,
    getAllPosts,
    getOnePost,
    setProperty,
    decComs,
    decLikes,
    incLikes,
    isValidePost,
}
