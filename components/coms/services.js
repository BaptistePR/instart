import db from "./model"
import Posts from "../posts/services"
import Users from "../users/services"
import Services from "../../middleWare/GenericServices"
import Query from "../../middleWare/QueryTools"
import Likes from "../likes/services"

async function getAllComs(filter) {
    console.log("coms::services::getAllComs")
    return await Services.getAllItems(db, filter)
}

async function getOneCom(filter) {
    console.log("coms::services::getOneCom")
    return await Services.getOneItem(db, filter)
}

async function isValideCom(com) {
    console.log("coms::services::isValideCom")
    if (com && Query.areDefined([com.postID, com.description, com.username, com.creationDate])) {
        // does this com has a proper post as its owner ?
        let owner = await Posts.getOnePost({_id: com.postID})
        if (!owner) {
            console.log("owner not found")
            return false
        }
        owner.coms += 1
        try{
            await owner.save()
        }catch(err){
            console.log("failed to update owner")
            console.log(err)
        }

        // does this com was sent by a known user ?
        let poster = await Users.getOneUser({username: com.username})
        if (!poster) {
            console.log("user not found")
            return false
        }

        com.likes = 0

        return true
    }else{
        console.log("body badly formed")
    }

    return false
}

async function get(req) {
    console.log("coms::services::get")
    return await Services.get(
        req,
        (com) => {
            return com._id.equals(req.query.id)
        },
        db)
}

async function getAll(req) {
    console.log("coms::services::getAll")
    return await Services.getAll(
        req,
        (com) => {
            return com.username === req.query.username
        },
        db)
}

async function add(com) {
    console.log("coms::services::add")
    return await Services.add(com, isValideCom, db)
}

async function getAllByPostId(req) {
    console.log("coms::services::getAllByPostId")
    return await Services.getAll(
        req,
        (com) => {
            return com.postID === req.query.postID
        },
        db)
}

async function remove(filter) {
    console.log("coms::services::remove")
    let coms = await getAllComs(filter)

    if (coms.length > 0) {
        for(let i = 0; i < coms.length; ++i){
            let com = coms[i]
            await Likes.remove({ownerID: com._id, isOnPost:false})
            await Posts.decComs(com.postID)
            await db.deleteOne({_id: com._id})
        }
    }else{
        console.log("No coms to delete")
    }

    return {deletedCount: coms.length}
}

async function setProperty(comID, property, behave){
    console.log("coms::services::setProperty")
    let com = await getOneCom({_id: comID})

    if(com){
        if(com[property] !== undefined && com[property] !== null){
            com[property] = behave(com[property])
            try{
                await com.save()
            }catch(err){
                console.log("could not update")
                console.log(err)
            }
            return 1
        }else{
            console.log("unknown property")
            return 2
        }
    }else{
        console.log("com not found")
        return 3
    }
}

async function decLikes(comID){
    console.log("coms::services::decComs")
    return setProperty(comID, "likes", (i) => {return i - 1})
}

async function incLikes(comID){
    console.log("coms::services::incLikes")
    return setProperty(comID, "likes", (i) => {return i + 1})
}

export default {
    get,
    getAll,
    add,
    remove,
    getAllByPostId,
    getAllComs,
    getOneCom,
    setProperty,
    decLikes,
    incLikes,
    isValideCom,
}
