import Mongoose from "mongoose"

const Schema = Mongoose.Schema

var ComsSchema = new Schema({
    postID: String,
    username: String,
    description: String,
    creationDate: Date,
    likes: Number,
})

ComsSchema.index({ postID: 1, creationDate: 2})
let Coms = Mongoose.model("Coms", ComsSchema)

export default Coms
