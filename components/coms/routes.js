import Coms from "./controller.js"
import express from "express"

const routes = express.Router()
const COMS_ROUTE = "/coms"

// get coms
routes.route(COMS_ROUTE).get(Coms.get)
// post coms
routes.route(COMS_ROUTE).post(Coms.add)
// delete coms
routes.route(COMS_ROUTE).delete(Coms.remove)

export default {
    routes,
    COMS_ROUTE,
}
