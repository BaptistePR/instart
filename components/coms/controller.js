import Coms from "./services"
import Answer from "../../middleWare/ResponseTools.js"
import Query from "../../middleWare/QueryTools.js"

async function get(req, res){
    console.log("coms::controller::get")
    let result = undefined
    let fun    = undefined
    
    if(!req.query.id && !req.query.postID){
        if(((req.query.start === undefined) + (req.query.length === undefined) == 1)){
            result = Answer.error(res, "If start or length is defined, the other needs to be")
        }else if(Query.areUndefined([req.query.start, req.query.length, req.query.username])){
            result = Answer.error(res, "Needs parameters defined : id OR (username AND/OR (start AND length)) OR postID")
        }else{
            fun = Coms.getAll
        }
    }else if(!req.query.postID){
        if(Query.areUndefined([req.query.start, req.query.length, req.query.username])){
            fun = Coms.get
        }else{
            result = Answer.error(res, "If id query is defined, no other should be")
        }
    }else{
        let tmp = (req.query.id === undefined) + (req.query.postID === undefined)
        if(tmp == 0){
            result = Answer.error(res, "id and postID can't be used together")
        }else{
            fun = Coms.getAllByPostId
        }
    }

    if(fun !== undefined){
        let coms = await fun(req, res)
            
        if(coms === undefined){
            result = Answer.success(res, [])//"item(s) not found"
        }else{
            result = Answer.success(res, coms)
        }
    }
	
    if(!result){
        result = Answer.error(res, "Needs parameters defined : id OR (username AND/OR (start AND length))")
    }

    return result
}

async function add(req, res){
    console.log("coms::controller::add")
    let result = await Coms.add(req.body)

    if(result !== undefined){
        return Answer.success(res, result)
    }else{
        return Answer.error(res, "Coms could not be added")
    }
}

async function remove(req, res){
    console.log("coms::controller::remove")
    
    let result = await Coms.remove(req.body)

    if(result){
        return Answer.success(res, result)
    }else{
        return Answer.error(res, "failed to remove")
    }
}

export default {
    get,
    add,
    remove
}
