import Mongoose from "mongoose"

const Schema = Mongoose.Schema

var UserSchema = new Schema({
    username: String,
    password: String,
})

UserSchema.index({ username: 1})
let User = Mongoose.model("User", UserSchema)

export default User
