import User from "./services"
import Answer from "../../middleWare/ResponseTools.js"
import Query from "../../middleWare/QueryTools.js"

async function get(req, res){
    console.log("Users::controller::get")
    let result = undefined
    let fun    = undefined
    
    if(!req.query.id){
        if((req.query.start === undefined) + (req.query.length === undefined) == 1){
            result = Answer.error(res, "If start or length is defined, the other needs to be")
        }else if(Query.areUndefined([req.query.start, req.query.length, req.query.username])){
            result = Answer.error(res, "Needs parameters defined : id OR (username AND/OR (start AND length))")
        }else{
            fun = User.getAll
        }
    }else{
        if(Query.areUndefined([req.query.start, req.query.length, req.query.username])){
            fun = User.get
        }else{
            result = Answer.error(res, "If id query is defined, no other should be")
        }
    }

    if(fun && !result){
        let users = await fun(req, res)
        if(!users){
            result = Answer.success(res, [])
        }else{
            result = Answer.success(res, users)
        }
    }

    return result
}


async function add(req, res){
    console.log("Users::controller::add")
    let result = await User.add(req.body)

    if(result !== undefined){
        return Answer.success(res, result)
    }else{
        return Answer.error(res, "User could not be added")
    }
}

async function remove(req, res){
    console.log("Users::controller::remove")
    let result = await User.remove(req.body)

    if(result){
        return Answer.success(res, result)
    }else{
        return Answer.serverError(res, "failed to remove")
    }
}

export default {
    get,
    add,
    remove,
}
