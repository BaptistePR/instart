import db from "./model"
import Services from "../../middleWare/GenericServices"
import Query from "../../middleWare/QueryTools"
import Security from "../../middleWare/IdentificationTools"
import Coms from "../coms/services"
import Likes from "../likes/services"
import Posts from "../posts/services"

async function getAllUsers(filter){
    console.log("User::services::getAllUsers")
    return await Services.getAllItems(db, filter)
}

async function getOneUser(filter){
    console.log("User::services::getOneUser")
    return await Services.getOneItem(db, filter)
}

async function isValideUser(user){
    console.log("User::services::isValideUser")
    if(user !== undefined && Query.areDefined([user.username, user.password])){
        let copy = await getOneUser({username: user.username})
	
        return !(copy)
    }

    return false
}

async function get(req){
    console.log("User::services::get")
    return await Services.get(
        req,
        (user) => {return user._id.equals(req.query.id)},
        db)
}

async function getAll(req){
    console.log("User::services::getAll")
    return await Services.getAll(
        req,
        (user) => {return user.username === req.query.username},
        db)
}

async function add(user){
    console.log("User::services::add")
    let result = await Services.add(user, isValideUser, db)

    if(result){
        result.password = Security.hashPassword(result.password)
        try{
            await result.save()
        }catch(err){
            console.log("failed to update user with hashed password")
            console.log(err)
        }
    }
    
    return result
    
}

async function remove(filter){
    console.log("User::services::remove")
    await Likes.remove({username: filter.username})
    await Coms.remove({username: filter.username})
    await Posts.remove({username: filter.username})
    return await Services.remove(filter, db)
}

export default {
    get,
    getAll,
    add,
    remove,
    getAllUsers,
    getOneUser,
    isValideUser,
}
