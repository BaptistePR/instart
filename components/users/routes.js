import User from "./controller"
import express from "express"

const routes = express.Router()
const USER_ROUTE = "/users"

// get user
routes.route(USER_ROUTE).get(User.get)
// post user
routes.route(USER_ROUTE).post(User.add)
// delete user
routes.route(USER_ROUTE).delete(User.remove)

export default {
    routes,
    USER_ROUTE,
}
