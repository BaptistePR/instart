import express from "express"
import bodyParser from "body-parser"
import Routes from "./components/index"
import mongoose from "mongoose"
import { MONGO, PORT } from "./conf/env"
import cors from "cors"

mongoose.connect(
    MONGO,
    { useNewUrlParser: true }
)
mongoose.set("useCreateIndex", true)

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error:"))
db.once("open", () => {
    console.log("Laura")
    const app = express()
    
    app.use(bodyParser.json())
    app.use(bodyParser.json({limit: "10mb"}))
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(cors())
    app.use("/", Routes)

    app.listen(PORT)
})
