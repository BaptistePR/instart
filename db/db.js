// API : /instazz/version/

/*
  posts/
      one post : :id

      range[start, start + length[ :
          start=:start&length=:length

      of 1 user :
          username=:username
      (2 ci dessus combinables)

      post : (rien)
      
      delete :
          :id
 */
exports.posts = [
    {
	id: 1,
	userID: 35,
	username: "Paul",
	title: "Barcelone",
	description: "Mes vacances",
	img:{
	    rel: "self",
	    href: "/img/barca.png",
	},
	coms: 1,
	// likes: 1
    },
]

/*
  coms/
      comme posts mais sans 'of 1 user'
 */
exports.coms = [
    {
	id: 1,
	idPost: 1,
	userID: 35,
	userName: "Paul",
	content: "tro swag",
	// likes: 1
    },
]

/*
  users/
      get : :id
      post (inscription) : (rien)
          infos dans le corp
      delete : (rien)
 */
exports.users = [
    {
	id: 1,
	username: "Paul",
	password: "PASSHASHED",
	img:{
	    rel: "self",
	    href: "/img/paul.png",
	},
	posts: [
	    {
		id: 1,
	    },
	],
    },
]

/*
const likes = [
    {
	id: 1,
	idPost: 1,
	idComs: -1, // not on com
	userID: 35,
	userName: "Paul",
    },
    {
        id: 2,
	idPost: 1,
	idComs: 1,
	userID: 35,
	userName: "Paul",
    },
]
*/

